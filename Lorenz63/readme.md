# Lorenz 63

## Memo
* OscillationとLorenzの共通ライブラリ
  * KalmanFilter.py
  * EnsembleKalmanFilter.py
  * SquareRootFilter.py
  * GenerateEnsemble.py
  * ParticleFilter.py
  * Visualization.py

## 説明
Lorenz方程式（Lorenz, 1963）を用いたデータ同化を理解するためのプログラム

Lorenz方程式：カオス的なふるまいを示す非線形常微分方程式の一種

## プログラムリスト
* Lorenz63_KF.ipynb
* Lorenz63_EnKF.ipynb
* Lorenz63_4DVAR.ipynb
* Lorenz63_PF.ipynb
* Lorenz63_SRF.ipynb
* Lorenz63.py
* Visualization.py
* KalmanFilter.py
* GenerateEnsemble.py
* EnsembleKalmanFilter.py
* ParticleFilter.py

## プログラムの詳細

### 教材
詳しくは各ノートブック参照

#### Lorenz63_KF.ipynb
Kalman Filterによるデータ同化

#### Lorenz63_EnKF.ipynb
Ensemble Kalman Filterによるデータ同化

#### Lorenz63_4DVAR.ipynb
4次元変分法によるデータ同化

#### Lorenz63_PF.ipynb
粒子フィルタによるデータ同化

#### Lorenz63_SRF.ipynb
平方根フィルタによるデータ同化

### ライブラリ
#### Lorenz63.py
減衰項つき振動の時間積分と接線形モデルの計算をするライブラリ

##### 使い方
__Lorenz63クラスをlorenzとしてインスタンス化する__

* Lorenz63モデルの変数
  * dt：時間積分のタイムステップ（default=0.001） 
  * s：ばね定数（default=10.0）
  * r：質量（default=28.0）
  * b：減衰定数（default=8.0/3.0）

* Lorenz63モデルの初期値
  * xi：初期のx方向の位置
  * yi：初期のy方向の位置
  * zi：初期のz方向の位置
  * noise：位置の誤差範囲
  * Pf:背景誤差共分散の初期値（default=None）
  
```
lorenz = Lorenz63.Lorenz63(xi,vi,noise,Pf,dt,k,mass,dump)
```

__dt秒後の位置と速度を得る__

* 戻り値の変数
  * x：dt秒後のx方向の位置
  * y：dt秒後のy方向の位置
  * z：dt秒後のz方向の位置
  * xo：観測誤差を加味したdt秒後のx方向の位置
  * yo：観測誤差を加味したdt秒後のy方向の位置（観測誤差は明示的に0にしている）
  * zo：観測誤差を加味したdt秒後のz方向の位置（観測誤差は明示的に0にしている）

```python
x, y, z = lorenz.time_integration()  
xo, yo, zo = lorenz.observation_noise() # osc.time_integration()のあとに実行する
```

__背景誤差共分散を更新する__

* 戻り値の変数
  * Pf：更新された背景誤差共分散

```
Pf = lorenz.transient_matrix()
```

#### Visualization.py
データを描画し、RMSEを計算するライブラリ

##### 使い方
__Visualizationクラスをvisとしてインスタンス化する__

* データ同化に用いた変数
  * obs：観測値
  * true：真値
  * sim：モデルの推定値
  * da：データ同化による推定値
  * interval：データ同化の間隔ステップ数
  * nt\_asm：データ同化に用いた時間ステップ数

* グラフの設定に用いる変数
  * name：グラフのyラベル
  * xlim：グラフのx軸の範囲
  
```
vis = Visualization.Visualization(obs,true,sim,da,interval,nt_asm,name,xlim)
```

__グラフの描画とRMSEの計算__

まず、グラフの描画にはfitメソッドを用いる

```
vis.fit()
```

次に、データ同化による値と観測値とのRMSEの計算にはrmseメソッドを用いる

```
vis.rmse()
```

#### KalmanFilter.py
Kalman Filterを実施するライブラリ

##### 使い方
__KalmanFilterクラスをkfとしてインスタンス化する__

* データ同化に用いた変数
  * xyz：ある時刻のモデルの位置の推定値
  * noise：観測誤差
  * Pf：背景誤差共分散
  * H：観測誤差
  * xyzobs：ある時刻の位置の観測値（真値に観測誤差が入ったもの）
  
```
kf = KalmanFilter.KalmanFilter(xyz,noise,Pf,H,xyzobs)
```

__Kalman Filterを実行する__

* 戻り値の変数
  * xyzs：データ同化した次の時刻の位置の予測値
  * Pf：更新された背景誤差共分散

```
xyzs, Pf = kf.fit()
```

#### GenerateEnsemble.py
初期のEnsembleを生成するライブラリ

##### 使い方
__GenerateEnsembleクラスをgeとしてインスタンス化する__

Ensembleは中心x、分散sの正規分布に従って生成される

* Ensemble生成に用いた変数
  * x：真値
  * n：生成するEnsemble数
  * s：Ensembleの分散
  
```
ge = GenerateEnsemble.GenerateEnsemble(x,n,s)
```

__Ensembleを生成する__

* 戻り値の変数
  * ens：生成されたEnsemble

```
ens = ge.fit()
```

#### EnsembleKalmanFilter.py
Ensemble Kalman Filterを実施するライブラリ

Ensembleメンバを用いて背景誤差共分散行列を計算する

##### 使い方
__EnsembleKalmanFilterクラスをekfとしてインスタンス化する__

* データ同化に用いた変数
  * ens：ある時刻のモデルの位置の推定値から生成したEnsemble
  * noise：観測誤差
  * H：観測誤差
  * xobs：ある時刻の位置の観測値（真値に観測誤差が入ったもの）
  
```
ekf = EnsembleKalmanFilter.EnsembleKalmanFilter(ens,H,xobs,noise)
```

__Ensemble Kalman Filterを実行する__

Kalman Filterと異なり、背景誤差共分散行列をEnsembleから計算する

* 戻り値の変数
  * enss：データ同化した次の時刻の速度の予測値

```
enss = ekf.fit()
```

#### ParticleFilter.py
粒子フィルタを実施するライブラリ

Ensembleメンバを用いて背景誤差共分散行列を計算する

##### 使い方
__ParticleFilterクラスをprfとしてインスタンス化する__

* データ同化に用いた変数
  * ens：ある時刻のモデルの位置の推定値から生成したEnsemble
  * noise：観測誤差
  * H：観測誤差
  * xobs：ある時刻の位置の観測値（真値に観測誤差が入ったもの）
  
```
prf = ParticleFilter.ParticleFilter(ens,H,xobs,noise)
```

__Particle Filterを実行する__

* 戻り値の変数
  * enss：データ同化した次の時刻の速度の予測値

```
enss = prf.fit()
```

#### SquareRootFilter.py
平方根フィルターを実施するライブラリ

Ensembleメンバを用いて背景誤差共分散行列を計算する

##### 使い方
__SquareRootFilterクラスをsrfとしてインスタンス化する__

* データ同化に用いた変数
  * ens：ある時刻のモデルの位置の推定値から生成したEnsemble
  * noise：観測誤差
  * H：観測誤差
  * xobs：ある時刻の位置の観測値（真値に観測誤差が入ったもの）
  
```
srf = SquareRootFilter.SquareRootFilter(ens,H,xobs,noise)
```

__Square Root Filterを実行する__

Ensemble Kalman Filterと同様に、背景誤差共分散行列をEnsembleから計算する

* 戻り値の変数
  * enss：データ同化した次の時刻の速度の予測値

```
enss = srf.fit()
```
