def cost(J, H, x, y, z, xobs, yobs, zobs, noise):
    if(len(H)==1 and H[0,0]==1):
        J = J + 0.5 * (xobs - x)**2 / noise
    elif(len(H)==1 and H[0,1]==1):
        J = J + 0.5 * (yobs - y)**2 / noise
    elif(len(H)==1 and H[0,2]==1):
        J = J + 0.5 * (zobs - z)**2 / noise
    elif(len(H)==2 and H[0,0]==1 and H[0,1]==1):
        J = J + 0.5 * (xobs - x)**2 / noise
        J = J + 0.5 * (yobs - y)**2 / noise
    elif(len(H)==2 and H[0,0]==1 and H[0,2]==1):
        J = J + 0.5 * (xobs - x)**2 / noise
        J = J + 0.5 * (zobs - z)**2 / noise
    elif(len(H)==2 and H[0,1]==1 and H[0,2]==1):
        J = J + 0.5 * (yobs - y)**2 / noise
        J = J + 0.5 * (zobs - z)**2 / noise
    else:
        J = J + 0.5 * (xobs - x)**2 / noise
        J = J + 0.5 * (yobs - y)**2 / noise
        J = J + 0.5 * (zobs - z)**2 / noise
    return J